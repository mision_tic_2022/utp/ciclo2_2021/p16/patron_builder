public class UsuarioBuilder {
    /*************
     * Atributos
     ************/
    private String nombre;
    private String apellido;
    private String email;
    private String telefono;
    private String direccion;

    //Constructor

    //Acciones/Métodos
    public UsuarioBuilder nombre(String nombre){
        this.nombre = nombre;
        return this;
    }
    
    public UsuarioBuilder apellido(String apellido){
        this.apellido = apellido;
        return this;
    }

    public UsuarioBuilder email(String email){
        this.email = email;
        return this;
    }

    public UsuarioBuilder telefono(String telefono){
        this.telefono = telefono;
        return this;
    }

    public UsuarioBuilder direccion(String direccion){
        this.direccion = direccion;
        return this;
    }


    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public String getEmail() {
        return email;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getDireccion() {
        return direccion;
    }


    //Construir objeto usuario
    public Usuario build(){
        return new Usuario(this);
    }

}
