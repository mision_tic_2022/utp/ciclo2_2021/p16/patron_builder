public class Usuario {
    /*************
     * Atributos
     ***********/
    private String nombre;
    private String apellido;
    private String email;
    private String telefono;
    private String direccion;

    // Constructor

    public Usuario(UsuarioBuilder builder) {
        if (builder.getNombre() == null || builder.getApellido() == null || builder.getEmail() == null
                || builder.getTelefono() == null) {
                    throw new IllegalArgumentException("Es requerida toda la información para construir un objeto de tipo Usuario");
        } else {
            this.nombre = builder.getNombre();
            this.apellido = builder.getApellido();
            this.email = builder.getEmail();
            this.telefono = builder.getTelefono();
            this.direccion = builder.getDireccion();
        }

    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Override
    public String toString() {
        return "Usuario [apellido=" + apellido + ", direccion=" + direccion + ", email=" + email + ", nombre=" + nombre
                + ", telefono=" + telefono + "]";
    }

    // Acciones de la clase

}