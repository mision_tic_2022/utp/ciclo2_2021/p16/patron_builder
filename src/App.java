public class App {
    public static void main(String[] args) throws Exception {
        String nombre = "Juan";
        String apellido = "Hernán";
        String email = "juan@gmail.com";
        String telefono = "123456";
        String direccion = "cra 100 # 56-43";
        
         UsuarioBuilder objBuilder = new UsuarioBuilder();
         objBuilder = objBuilder.nombre(nombre);
         objBuilder = objBuilder.apellido(apellido);
         objBuilder = objBuilder.email(email);
         objBuilder = objBuilder.telefono(telefono);
         objBuilder = objBuilder.direccion(direccion);

         
         Usuario objUsuario = new UsuarioBuilder().nombre(nombre).apellido(apellido).email(email).telefono(telefono).direccion(direccion).build();

         System.out.println(objUsuario);

    }
}
